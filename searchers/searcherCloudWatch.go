package searchers

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	"strings"
)

var (
	cloudWatchMap         map[string]string
	cloudWatchServiceName = "CloudWatch"
)

// SearchForSuspectsCloudWatch takes in a suspect string and a
// pointer to a session struct and returns a map of
// [<ARN/Id/etc>] = CloudWatch:<resource-type>
func SearchForSuspectsCloudWatch(suspect string, sess *session.Session) map[string]string {
	fmt.Println("-Searching", cloudWatchServiceName, "...")

	cloudWatchMap = make(map[string]string)

	svc := cloudwatchlogs.New(sess)

	searchForCloudWatchLogGroups(suspect, svc)

	return cloudWatchMap
}

// CloudWatchLogs API
func searchForCloudWatchLogGroups(suspect string, svc *cloudwatchlogs.CloudWatchLogs) {
	var resourceType = "LogGroup"
	var searchPropertyPath = "LogGroupName"
	var searchField = cloudWatchServiceName + ":" + resourceType + "." + searchPropertyPath

	input := &cloudwatchlogs.DescribeLogGroupsInput{}

	result, err := svc.DescribeLogGroups(input)

	if err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.LogGroups {
		if strings.Contains(*element.LogGroupName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.LogGroupName)
			// [<log-group-name>] = CloudWatch:LogGroup
			cloudWatchMap[*element.LogGroupName] = cloudWatchServiceName + ":" + resourceType
		}
	}
}
