package searchers

import (
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

var (
	s3Map         map[string]string
	s3ServiceName = "S3"
)

// SearchForSuspectsS3 takes in a suspect string and a
// pointer to a session struct and returns a map of
// [<ARN/Id/etc>] = S3:<resource-type>
func SearchForSuspectsS3(suspect string, sess *session.Session) map[string]string {
	fmt.Println("-Searching", s3ServiceName, "...")

	s3Map = make(map[string]string)

	svc := s3.New(sess)

	searchForS3Buckets(suspect, svc)

	return s3Map
}

func searchForS3Buckets(suspect string, svc *s3.S3) {
	var resourceType = "Bucket"
	var searchPropertyPath = "Name"
	var searchField = s3ServiceName + ":" + resourceType + "." + searchPropertyPath

	input := &s3.ListBucketsInput{}

	result, err := svc.ListBuckets(input)

	if err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.Buckets {
		if strings.Contains(*element.Name, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.Name)
			// [<bucket name>] = S3:Bucket
			s3Map[*element.Name] = s3ServiceName + ":" + resourceType
		}
	}
}
