package searchers

import (
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

var (
	iamMap         map[string]string
	iamServiceName = "IAM"
)

// SearchForSuspectsIAM takes in a suspect string and a
// pointer to a session struct and returns a map of
// [<ARN/Id/etc>] = IAM:<resource-type>
func SearchForSuspectsIAM(suspect string, sess *session.Session) map[string]string {
	fmt.Println("-Searching", iamServiceName, "...")

	iamMap = make(map[string]string)

	svc := iam.New(sess)

	searchForIAMUsers(suspect, svc)
	searchForIAMInstanceProfiles(suspect, svc)
	searchForIAMRoles(suspect, svc)
	searchForIAMPolicies(suspect, svc)

	return iamMap
}

func searchForIAMUsers(suspect string, svc *iam.IAM) {
	var resourceType = "User"
	var searchPropertyPath = "UserName"
	var searchField = iamServiceName + ":" + resourceType + "." + searchPropertyPath
	result, err := svc.ListUsers(&iam.ListUsersInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.Users {
		if strings.Contains(*element.UserName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.UserName)
			// [<user-name] = IAM:User
			iamMap[*element.UserName] = iamServiceName + ":" + resourceType
		}
	}
}

func searchForIAMInstanceProfiles(suspect string, svc *iam.IAM) {
	var resourceType = "InstanceProfile"
	var searchPropertyPath = "InstanceProfileName"
	var searchField = iamServiceName + ":" + resourceType + "." + searchPropertyPath

	input := &iam.ListInstanceProfilesInput{}
	result, err := svc.ListInstanceProfiles(input)

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.InstanceProfiles {
		if strings.Contains(*element.InstanceProfileName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.InstanceProfileName)
			// [<instance-profile-name>] = IAM:InstanceProfile
			iamMap[*element.InstanceProfileName] = iamServiceName + ":" + resourceType
		}
	}
}

func searchForIAMRoles(suspect string, svc *iam.IAM) {
	var resourceType = "Role"
	var searchPropertyPath = "RoleName"
	var searchField = iamServiceName + ":" + resourceType + "." + searchPropertyPath

	input := &iam.ListRolesInput{}

	result, err := svc.ListRoles(input)

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.Roles {
		if strings.Contains(*element.RoleName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.RoleName)
			// [<role-name>] = IAM:Role
			iamMap[*element.RoleName] = iamServiceName + ":" + resourceType
		}
	}
}

func searchForIAMPolicies(suspect string, svc *iam.IAM) {
	var resourceType = "Policy"
	var searchPropertyPath = "PolicyName"
	var searchField = iamServiceName + ":" + resourceType + "." + searchPropertyPath

	input := &iam.ListPoliciesInput{}

	result, err := svc.ListPolicies(input)

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.Policies {
		if strings.Contains(*element.PolicyName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.PolicyName)
			// [<policy-arn] = IAM:Policy
			iamMap[*element.Arn] = iamServiceName + ":" + resourceType
		}
	}
}
