package searchers

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"strings"
)

var (
	ec2Map         map[string]string
	ec2ServiceName = "EC2"
)

// SearchForSuspectsEC2 takes in a suspect string and a
// pointer to a session struct and returns a map of
// [<ARN/Id/etc>] = EC2:<resource-type>
func SearchForSuspectsEC2(suspect string, sess *session.Session) map[string]string {
	fmt.Println("-Searching", ec2ServiceName, "...")

	ec2Map = make(map[string]string)

	svc := ec2.New(sess)

	searchForEC2Instances(suspect, svc)
	searchForEC2LaunchTemplates(suspect, svc)
	searchForEC2Volumes(suspect, svc)
	searchForEC2Addresses(suspect, svc)
	searchForEC2SecurityGroups(suspect, svc)
	searchForEC2KeyPairs(suspect, svc)
	searchForEC2NetworkInterfaces(suspect, svc)
	searchForEC2InternetGateways(suspect, svc)
	searchForEC2VPCs(suspect, svc)
	searchForEC2VPCPeeringConnections(suspect, svc)

	return ec2Map
}

func searchForEC2Instances(suspect string, svc *ec2.EC2) {
	var resourceType = "Instance"
	var searchPropertyPath = "Tag"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeInstances(&ec2.DescribeInstancesInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.Reservations {
		for _, nestedElement := range element.Instances {
			for _, subNestedElement := range nestedElement.Tags {
				if strings.Contains(*subNestedElement.Value, suspect) {
					fmt.Println("---* A [", suspect, "] suspect found:", *subNestedElement.Value, " in Instance ", *nestedElement.InstanceId)
					// [instance-id] = EC2:Instance
					ec2Map[*nestedElement.InstanceId] = ec2ServiceName + ":" + resourceType
				}
			}
		}
	}
}

func searchForEC2LaunchTemplates(suspect string, svc *ec2.EC2) {
	var resourceType = "LaunchTemplate"
	var searchPropertyPath = "LaunchTemplateName"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeLaunchTemplates(&ec2.DescribeLaunchTemplatesInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.LaunchTemplates {
		if strings.Contains(*element.LaunchTemplateName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.LaunchTemplateName)
			// [<launch-template-id] = EC2:LaunchTemplate
			ec2Map[*element.LaunchTemplateId] = ec2ServiceName + ":" + resourceType
		}
	}
}

func searchForEC2Volumes(suspect string, svc *ec2.EC2) {
	var resourceType = "Volume"
	var searchPropertyPath = "Tag"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath
	result, err := svc.DescribeVolumes(&ec2.DescribeVolumesInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.Volumes {
		for _, nestedElement := range element.Tags {
			if strings.Contains(*nestedElement.Value, suspect) {
				fmt.Println("---* A [", suspect, "] suspect found:", *nestedElement.Value, " in Volume ", *element.VolumeId)
				// [volume-id] = EC2:Volume
				ec2Map[*element.VolumeId] = ec2ServiceName + ":" + resourceType
			}
		}
	}
}

func searchForEC2Addresses(suspect string, svc *ec2.EC2) {
	var resourceType = "Address"
	var searchPropertyPath = "Tag"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeAddresses(&ec2.DescribeAddressesInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.Addresses {
		for _, nestedElement := range element.Tags {
			if strings.Contains(*nestedElement.Value, suspect) {
				fmt.Println("---* A [", suspect, "] suspect found:", *nestedElement.Value, " in Address ", *element.PublicIp)
				// [association-id] = EC2:Address
				ec2Map[*element.AssociationId] = ec2ServiceName + ":" + resourceType
			}
		}
	}
}

func searchForEC2KeyPairs(suspect string, svc *ec2.EC2) {
	var resourceType = "KeyPair"
	var searchPropertyPath = "KeyName"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeKeyPairs(&ec2.DescribeKeyPairsInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.KeyPairs {
		if strings.Contains(*element.KeyName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.KeyName)
			// [key-name] = EC2:KeyPair
			ec2Map[*element.KeyName] = ec2ServiceName + ":" + resourceType
		}
	}
}

func searchForEC2SecurityGroups(suspect string, svc *ec2.EC2) {
	var resourceType = "SecurityGroup"
	var searchPropertyPath = "GroupName"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeSecurityGroups(&ec2.DescribeSecurityGroupsInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.SecurityGroups {
		if strings.Contains(*element.GroupName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.GroupName)
			// [group-id] = EC2:SecurityGroup
			ec2Map[*element.GroupId] = ec2ServiceName + ":" + resourceType
		}
	}
}

func searchForEC2NetworkInterfaces(suspect string, svc *ec2.EC2) {
	var resourceType = "NetworkInterface"
	var searchPropertyPath string
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeNetworkInterfaces(&ec2.DescribeNetworkInterfacesInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	searchPropertyPath = "Description"
	fmt.Println("--Searching", searchField)
	for _, element := range result.NetworkInterfaces {
		if strings.Contains(*element.Description, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.Description, " in NetworkInterface ", *element.NetworkInterfaceId)
			// [interface-id] = EC2:NetworkInterface
			ec2Map[*element.NetworkInterfaceId] = ec2ServiceName + ":" + resourceType
		}
	}

	searchPropertyPath = "Tag"
	fmt.Println("--Searching EC2:NetworkInterface.Tag.Value...")
	for _, element := range result.NetworkInterfaces {
		for _, nestedElement := range element.TagSet {
			if strings.Contains(*nestedElement.Value, suspect) {
				fmt.Println("---* A [", suspect, "] suspect found:", *nestedElement.Value, " in NetworkInterface ", *element.NetworkInterfaceId)
				// [interface-id] = EC2:NetworkInterface
				ec2Map[*element.NetworkInterfaceId] = ec2ServiceName + ":" + resourceType
			}
		}
	}
}

func searchForEC2InternetGateways(suspect string, svc *ec2.EC2) {
	var resourceType = "InternetGateway"
	var searchPropertyPath = "Tag"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeInternetGateways(&ec2.DescribeInternetGatewaysInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.InternetGateways {
		for _, nestedElement := range element.Tags {
			if strings.Contains(*nestedElement.Value, suspect) {
				fmt.Println("---* A [", suspect, "] suspect found:", *nestedElement.Value, " in InternetGateway ", *element.InternetGatewayId)
				// [internet-gateway-id] = EC2:InternetGateway
				ec2Map[*element.InternetGatewayId] = ec2ServiceName + ":" + resourceType
			}
		}
	}
}

func searchForEC2VPCs(suspect string, svc *ec2.EC2) {
	var resourceType = "VPC"
	var searchPropertyPath = "Tag"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeVpcs(&ec2.DescribeVpcsInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.Vpcs {
		for _, nestedElement := range element.Tags {
			if strings.Contains(*nestedElement.Value, suspect) {
				fmt.Println("---* A [", suspect, "] suspect found:", *nestedElement.Value, " in VPC ", *element.VpcId)
				// [<vpc-id>] = EC2:InternetGateway
				ec2Map[*element.VpcId] = ec2ServiceName + ":" + resourceType
			}
		}
	}
}

func searchForEC2VPCPeeringConnections(suspect string, svc *ec2.EC2) {
	var resourceType = "VPCPeeringConnection"
	var searchPropertyPath = "Tag"
	var searchField = ec2ServiceName + ":" + resourceType + "." + searchPropertyPath

	result, err := svc.DescribeVpcPeeringConnections(&ec2.DescribeVpcPeeringConnectionsInput{})

	if err != nil {
		fmt.Println("Error", err)
		return
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.VpcPeeringConnections {
		for _, nestedElement := range element.Tags {
			if strings.Contains(*nestedElement.Value, suspect) {
				fmt.Println("---* A [", suspect, "] suspect found:", *nestedElement.Value, " in VPCPeeringConnection ", *element.VpcPeeringConnectionId)
				// [<vpc-peering-connection-id>] = EC2:VPCPeeringConnection
				ec2Map[*element.VpcPeeringConnectionId] = ec2ServiceName + ":" + resourceType
			}
		}
	}
}
