package searchers

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/acm"
	"strings"
)

var (
	acmMap         map[string]string
	acmServiceName = "ACM"
)

// SearchForSuspectsACM takes in a suspect string and a
// pointer to a session struct and returns a map of
// [<ARN/Id/etc>] = ACM:<resource-type>
func SearchForSuspectsACM(suspect string, sess *session.Session) map[string]string {
	fmt.Println("-Searching", acmServiceName, "...")

	acmMap = make(map[string]string)

	svc := acm.New(sess)

	searchForACMCertificates(suspect, svc)

	return acmMap
}

func searchForACMCertificates(suspect string, svc *acm.ACM) {
	var resourceType = "Certificate"
	var searchPropertyPath = "DomainName"
	var searchField = acmServiceName + ":" + resourceType + "." + searchPropertyPath

	input := &acm.ListCertificatesInput{}

	result, err := svc.ListCertificates(input)

	if err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println("--Searching", searchField)
	for _, element := range result.CertificateSummaryList {
		if strings.Contains(*element.DomainName, suspect) {
			fmt.Println("---* A [", suspect, "] suspect found:", *element.DomainName)
			// [<certificate-arn>] = ACM:Certificate
			acmMap[*element.CertificateArn] = acmServiceName + ":" + resourceType
		}
	}
}
