package api

import (
	"errors"
	"gitlab.com/mgable/suspects/suspectsearch"
)

func Search(suspect string) (map[string]string, error) {

	// TODO: Any universal validation should be done in one place
	if len(suspect) < 3 {
		return nil, errors.New("Suspect string must be three or more characters")
	}

	suspects, err := suspectsearch.SearchForSuspects(suspect)
	if err != nil {
		return nil, errors.New("Invalid suspect string")
	} else {
		return suspects, nil
	}
}
