package api_test

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/google/uuid"
	"gitlab.com/mgable/suspects/api"
	"testing"
)

// suspects testing suite. This suite
// performs AWS operations via your
// default configurations and may incur
// some cost.

// A test search using the api package.
// Should pass whether resource exists
// or not. Just a test of basic operation
func TestSuspects(t *testing.T) {
	_, err := api.Search("aeris")
	if err != nil {
		t.Fail()
		t.Log("api.Search failed")
	}
}

// Pass in an invalid suspect string
func TestSuspectsToShort(t *testing.T) {
	suspects, err := api.Search("ab")

	if err != nil {
		fmt.Print("Suspects map: ", suspects)
		fmt.Print(err)
	} else {
		t.Fail()
		t.Log("Did not receive expected error")
	}
}

// Create a random suspect and make
// sure the suspect is found
func TestDetectionOfSuspect(t *testing.T) {
	uuid, _ := uuid.NewRandom()
	var testUser = "jenova-" + uuid.String()
	sess, sessionErr := session.NewSessionWithOptions(session.Options{
		// Using ~/.aws/config defaults (~/.aws/credentials also used by default)
		SharedConfigState: session.SharedConfigEnable,
	})

	if sessionErr != nil {
		t.Fail()
		t.Log("Can't create new session: ", sessionErr)
	}

	svc := iam.New(sess)
	input := &iam.CreateUserInput{UserName: aws.String(string(testUser))}
	t.Log("Creating a ", testUser, "user...")
	svc.CreateUser(input)
	inputSearchResult, inputSearchError := api.Search(testUser)

	if inputSearchError != nil {
		t.Fail()
		t.Log("Search failed: ", inputSearchError)
	} else {
		if len(inputSearchResult) != 1 {
			t.Fail()
			t.Log("Suspects result map did not have expected length of 1, but ", len(inputSearchResult))
		}
	}

	// Cleanup
	deleteInput := &iam.DeleteUserInput{UserName: aws.String(string(testUser))}
	svc.DeleteUser(deleteInput)

	deleteSearchResult, deleteSearchError := api.Search(testUser)

	if deleteSearchError != nil {
		t.Fail()
		t.Log("Cleanup search failed: ", deleteSearchError)
	} else {
		if len(deleteSearchResult) != 0 {
			t.Fail()
			t.Log("Cleanup failed. Suspects result map did not have expected length of 0, but ", len(deleteSearchResult))
		}
	}
}
