# suspects

A tool that provides a list of resources based on a keyword without manipulating your deployment

One keyword. One search. One list of suspects. No resources are harmed by running `suspects`

`suspects` looks through your AWS account for build-blocking, lingering AWS resources and can be run in 1) standaline mode via `go run main.go --suspect <suspect>`, which lists suspets in your terminal or 2) via api (using the `api` package), which lists them in a console output and also gives you a map of arn/id/name to suspect you can use to do further processing, reporting, etc. 

## Project motivation
The goal is to be able to use this list to learn of lingering resources from problematic build teardowns before any developers waste time with a subsequent build that fails due to these resources. Many tools seek to offer configuration for deleting resources, which is important. However, `suspects` adds something a little bit different. We acknowledge that in large builds, developers can't know every resource and every state it's in. Sometimes we might not know a blocking resource has lingered until our build fails, so knowing what problems to look for beforehand becomes difficult, especially in enterprise-level builds. This is an opinionated subset/filter for AWS resources. Requests for features and resource support are encouraged!

## 1) Running as a standalone program
First download and unzip this code. Then navigate to the `suspects` directory that holds the `main.go` file. The following comand will allow you to get a list of resources that might contain your suspect string: `go run main.go --suspect <suspect-string>`

## Sample output for `go run main.go --suspect aeris` (assuming you had an aeris resource)
```
-Searching S3...
--Searching S3:Bucket.Name...
-Searching IAM...
--Searching IAM:User.UserName...
--Searching IAM:InstanceProfile.InstanceProfileName...
--Searching IAM:Role.RoleName...
--Searching IAM:Policy.PolicyName...
-Searching EC2...
--Searching EC2:Reservation:Instance.Tag.Value...
--Searching EC2:LaunchTemplate:Instance.LaunchTemplateName...
--Searching EC2:Volume.Tag.Value...
--Searching EC2:Address.Tag.Value...
--Searching EC2:SecurityGroup.GroupName...
--Searching EC2:KeyPair.KeyPairInfo.KeyName...
---* A [ aeris ] suspect found: aeris
--Searching EC2:NetworkInterface.Description...
--Searching EC2:NetworkInterface.Tag.Value...
--Searching EC2:InternetGateway.Tag.Value...
--Searching EC2:VPC.Tag.Value...
--Searching EC2:VPCPeeringConnection.Tag.Value...
-Searching ACM...
--Searching ACM:CertificateSummary.DomainName...
```

## 2) Using `suspects` in your Go code
- `go get -u gitlab.com/mgable/suspects/...`
- You can import `suspects` via its `api` package and use the `Search` method.

```
import "gitlab.com/mgable/suspects/api"
...
  // Suspect string must be three or more charaters
  resultMap, err := api.Search("sephiroth")
...
```

## API
The `suspects` api delivers a map of suspects with the format `[resource-id/arn/etc]"<ServiceName>:<ResourceType>"`, so a suspect bucket might look like `[my-bucket-name]"S3:Bucket"`. The keys delivered are based on the values necessary for deletion per the AWS CLI Command Reference; the values are simply the service and resource type found. 

## Currently supported resources search by suspects
| resource-id/arn/etc       | Service:ResourceType     |
|---------------------------|--------------------------|
| certificate-arn           | ACM:Certificate          |
| log-group-name            | CloudWatch:LogGroup      |
| association-id            | EC2:Address              |
| instance-id               | EC2:Instance             |
| internet-gateway-id       | EC2:InternetGateway      |
| key-name                  | EC2:KeyPair              |
| launch-template-id        | EC2:LaunchTemplate       |
| interface-id              | EC2:NetworkInterface     |
| group-id                  | EC2:SecurityGroup        |
| volume-id                 | EC2:Volume               |
| vpc-id                    | EC2:VPC                  |
| vpc-peering-connection-id | EC2:VPCPeeringConnection |
| instance-profile-name     | IAM:InstanceProfile      |
| policy-arn                | IAM:Policy               |
| role-name                 | IAM:Role                 |
| user-name                 | IAM:User                 |
| bucket-name               | S3:Bucket                |

## Upcoming features
- Support for more services and resources (always ongoing)
- Increase test coverage

TIPS:
- `suspects` only lists resources, and is safe to be used against any deployment.
- `suspects` works best if you use a similar naming convention string in as many places as possible for every build
- `suspects` is most useful to run after a teardown or before a subsequent build that could be affected by lingering resources
- As with many other AWS cleaning tools, the standard warning applies: a future feature will return a list of resources and `suspects` developers can't be responsible for any mayhem that results from trying to use that list for automated/scripted deletes, etc.