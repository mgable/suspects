module gitlab.com/mgable/suspects

go 1.13

require (
	github.com/aws/aws-sdk-go v1.30.4
	github.com/google/uuid v1.1.1
)
