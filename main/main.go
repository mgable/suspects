package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/mgable/suspects/suspectsearch"
)

var (
	suspect string
)

// Intake the suspect name from the command line,
// and do some length validation
func init() {
	flag.StringVar(&suspect, "suspect", "", "Usage: go run main.go -suspect <suspect-string>")

	flag.Parse()

	if suspect == "" || len(suspect) < 3 {
		fmt.Println("Usage: go run suspect.go -suspect <suspect-string > 2 letters>")
		os.Exit(3)
	}

	fmt.Println("suspect is searching AWS for:[", suspect, "]resources in AWS...")
}

// go run main.go --suspect <your-suspect>
// Printed/terminal information only. To get a map
// of found suspects programatically, use the API:
// (gitlab.com/mgable/suspects/api)
func main() {
	suspectsearch.SearchForSuspects(suspect)
}
