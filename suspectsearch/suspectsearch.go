package suspectsearch

import (
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"gitlab.com/mgable/suspects/searchers"
	"log"
)

// SearchForSuspects searches an AWS account based on your default credentials
// for resources that have a match of the provided suspect string.
// Returns a map of [<resource-id/arn/ect>] = "<Service>:<ResourceType>"
func SearchForSuspects(suspect string) (map[string]string, error) {
	sess, err := session.NewSessionWithOptions(session.Options{
		// Using ~/.aws/config defaults (~/.aws/credentials also used by default)
		SharedConfigState: session.SharedConfigEnable,
	})

	if err != nil {
		log.Fatal(err)
		return nil, errors.New("Error creating session")
	}

	result, err := sess.Config.Credentials.Get()
	if err != nil {
		log.Fatal(err)
		return nil, errors.New("Error getting credentials")
	}

	fmt.Println("Using credentials: ", result)

	//Searching services for suspect
	var locals3Map map[string]string = searchers.SearchForSuspectsS3(suspect, sess)
	var localIamMap map[string]string = searchers.SearchForSuspectsIAM(suspect, sess)
	var localEc2Map map[string]string = searchers.SearchForSuspectsEC2(suspect, sess)
	var localAcmMap map[string]string = searchers.SearchForSuspectsACM(suspect, sess)
	var localCloudWatchMap map[string]string = searchers.SearchForSuspectsCloudWatch(suspect, sess)

	var localMap = make(map[string]string)
	//Merge maps
	for k, v := range locals3Map {
		localMap[k] = v
	}
	for k, v := range localIamMap {
		localMap[k] = v
	}
	for k, v := range localEc2Map {
		localMap[k] = v
	}
	for k, v := range localAcmMap {
		localMap[k] = v
	}
	for k, v := range localCloudWatchMap {
		localMap[k] = v
	}

	return localMap, nil
}
